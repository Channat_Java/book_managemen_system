package kshrd.models.dto;

public class BookImage {

    private Integer id;
    private Integer bookId;
    private String imageUrl;
    private Boolean status;

    public BookImage() {
    }

    public BookImage(Integer id, Integer bookId, String imageUrl, Boolean status) {
        this.id = id;
        this.bookId = bookId;
        this.imageUrl = imageUrl;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BookImage{" +
                "id=" + id +
                ", bookId=" + bookId +
                ", imageUrl='" + imageUrl + '\'' +
                ", status=" + status +
                '}';
    }
}
