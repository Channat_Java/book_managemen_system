package kshrd.models.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kshrd.models.dto.Role;
import kshrd.models.dto.User;

import javax.xml.transform.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private BMSConnection bmsConnection = new BMSConnection();
    private PreparedStatement preparedStatement;
    public static User userLogedin;

    public User userLogin(User user) {

        try {
            bmsConnection.openConnection();
            String sql = "SELECT * FROM bms_users " +
                    "WHERE username=? AND password = md5(?) AND status IS TRUE;";
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userLogedin = new User();
                userLogedin.setId(resultSet.getInt(1));
                userLogedin.setUsername(resultSet.getString(2));
            }

            if (userLogedin != null) {
                preparedStatement.close();

                // take userLogin Id to find all roles
                String sqlFindRole = "SELECT r.id, r.role_name " +
                        "FROM bms_user_roles ur " +
                        "JOIN bms_roles r ON ur.role_id = r.id " +
                        "WHERE user_id = ?;";
                preparedStatement = bmsConnection.connection.prepareStatement(sqlFindRole);
                preparedStatement.setInt(1, userLogedin.getId());
                ResultSet roleResultSet = preparedStatement.executeQuery();
                Role role;
                List<Role> roleList = new ArrayList<>();
                while (roleResultSet.next()) {
                    role = new Role();
                    role.setId(roleResultSet.getInt(1));
                    role.setName(roleResultSet.getString(2));
                    roleList.add(role);
                }
                userLogedin.setRoles(roleList);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }
        return userLogedin;
    }

    public ObservableList<User> userList() {
        ObservableList<User> users = FXCollections.observableArrayList();
        try {
            bmsConnection.openConnection();
            String sql = "SELECT id, username FROM bms_users " +
                    "WHERE status IS TRUE ORDER BY id";
            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(1));
                user.setUsername(resultSet.getString(2));
                users.add(user);
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }

        return users;
    }

    public List<String> roleList() {
        List<String> roles = new ArrayList<>();

        try {
            bmsConnection.openConnection();
            String sql = "SELECT role_name FROM bms_roles";

            preparedStatement = bmsConnection.connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roles.add(resultSet.getString(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bmsConnection.closeConnection();
        }

        return roles;
    }

}
