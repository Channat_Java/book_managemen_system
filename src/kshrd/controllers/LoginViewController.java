package kshrd.controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import kshrd.models.dao.UserDAO;
import kshrd.models.dto.User;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginViewController implements Initializable {

    private UserDAO userDAO = new UserDAO();

    @FXML
    private JFXTextField usernameField;

    @FXML
    private JFXPasswordField passwordField;

    @FXML
    private Label incorrectLabel;

    @FXML
    void reloadButtonPressed() {

    }

    @FXML
    void loginButtonPressed() {

        String username = usernameField.getText().trim();
        String password = passwordField.getText().trim();

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        /* check if userLogin is not null then allow open application,
         if not show incorrectLabel */
        if (userDAO.userLogin(user) != null) {
            // Load Main Screen
            MainScreenController mainScreenController = new MainScreenController();
            mainScreenController.loadMainScreen();
            System.out.println(userDAO.userLogedin);
        } else {
            incorrectLabel.setVisible(true);
        }

    }

    @FXML
    void usernameFieldKeyType() {
        incorrectLabel.setVisible(false);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        incorrectLabel.setVisible(false);
    }
}
