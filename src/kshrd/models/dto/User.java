package kshrd.models.dto;

import java.sql.Date;
import java.util.List;

public class User {

    private Integer id;
    private String username;
    private String password;
    private Boolean status;
    private Date registerDate;
    private List<Role> roles;

    public User() {
    }

    public User(Integer id, String username, String password, Boolean status, Date registerDate, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.status = status;
        this.registerDate = registerDate;
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", registerDate=" + registerDate +
                ", roles=" + roles +
                '}';
    }
}
