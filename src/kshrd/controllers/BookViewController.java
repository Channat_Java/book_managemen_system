package kshrd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import kshrd.models.dao.BookDAO;
import kshrd.models.dto.Author;
import kshrd.models.dto.Book;
import kshrd.models.dto.BookImage;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

public class BookViewController implements Initializable {


    @FXML
    private JFXButton deleteButton;

    @FXML
    private JFXComboBox<String> authorNameComboBox;

    @FXML
    private JFXTextField titleField;

    @FXML
    private JFXButton editButton;

    @FXML
    private JFXButton newButton;

    @FXML
    private JFXTextArea descriptionField;

    @FXML
    private JFXButton saveButton;

    @FXML
    private JFXButton uploadButton;

    @FXML
    private TableView<BookImage> imageTableView;

    @FXML
    private TableColumn<BookImage, String> imageColumn;

    @FXML
    private TableView<Book> bookTableView;

    @FXML
    private TableColumn<Book, Integer> idColumn;
    @FXML
    private TableColumn<Book, String> titleColumn;
    @FXML
    private TableColumn<Book, String> authorColumn;





    private BookDAO bookDAO = new BookDAO();
    List<File> files = new ArrayList<>();





    @FXML
    void uploadButtonPressed() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("PDF", "*.pdf");
        fileChooser.getExtensionFilters().add(filter);
        // get multiple files from files we have selected
        files = fileChooser.showOpenMultipleDialog(null);
        if (!files.isEmpty()) {
            imageColumn.setCellValueFactory(new PropertyValueFactory<>("imageUrl"));
            files.forEach( file -> {
                BookImage bookImage = new BookImage();
                bookImage.setImageUrl(file.getName());
                imageTableView.getItems().add(bookImage);
            });


        }
    }

    @FXML
    void newButtonPressed() {

    }

    @FXML
    void saveButtonPressed() {
        Book book = new Book();
        book.setTitle(titleField.getText().trim());
        book.setDescription(descriptionField.getText().trim());
        Author author = new Author();
        author.setFullName(authorNameComboBox.getSelectionModel().getSelectedItem());
        book.setAuthor(author);
        List<BookImage> bookImages = new ArrayList<>();

        if (!files.isEmpty() && files != null) {
            for (File file : files) {
                new Thread(() -> {
                    BookImage bookImage = new BookImage();
                    System.out.println("writing file....");
                    String fileName = file.getName();
                    String fileExtension = fileName.substring(fileName.lastIndexOf("."));
                    fileName = UUID.randomUUID().toString() + fileExtension;
                    String uploadLocation = "/Users/temchannat/Documents/Java FX/Book Management System/book_images/";
                    File path = new File(uploadLocation);
                    if (!path.exists())
                        path.mkdirs();
                    bookImage.setImageUrl(uploadLocation + fileName);
                    bookImages.add(bookImage);
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        FileOutputStream fileOutputStream = new FileOutputStream(uploadLocation+fileName);
                        int c ;
                        while ((c=fileInputStream.read()) != -1) {
                            fileOutputStream.write(c);
                        }
                        System.out.println("Finish writing......");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }
        book.setBookImages(bookImages);

        bookDAO.insertBook(book);

    }

    @FXML
    void editButtonPressed() {

    }

    @FXML
    void deleteButtonPressed() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        authorNameComboBox.getItems().addAll(bookDAO.authorListName());

        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        Author author = new Author();
        String fullName = author.getFullName();
        authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));

        bookTableView.setItems(bookDAO.bookObservableList());

        bookDAO.bookObservableList().forEach(books -> {
            System.out.println(books.getAuthor().getFullName());
        });


    }
}
