package kshrd;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import kshrd.controllers.MainScreenController;

public class Main extends Application {




    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("views/LoginView.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        Image image = new Image(getClass().getResourceAsStream("views/images/books-stack-of-three.png"));
        stage.getIcons().add(image);
        stage.setScene(scene);
        stage.setTitle("Login");
        stage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
