package kshrd.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import kshrd.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {



    @FXML
    public BorderPane mainLayout;

    @FXML
    private Button authorButton;

    @FXML
    private Button bookButton;

    @FXML
    private Button logoutButton;

    @FXML
    private Button userButton;


    public void loadMainScreen() {
        try {
            mainLayout = FXMLLoader.load(getClass()
                    .getResource("../views/MainScreen.fxml"));
            Scene scene = new Scene(mainLayout);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Book Management System");
            stage.setMinHeight(500);
            stage.setMinWidth(700);
            stage.show();
            loadSubView("../views/BookView.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadSubView(String location) {
        try {
            mainLayout.setCenter(new FXMLLoader().load(getClass().getResource(location)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void bookButtonPressed(ActionEvent event) {
        loadSubView("../views/BookView.fxml");
    }

    @FXML
    void authorButtonPress(ActionEvent event) {
        loadSubView("../views/AuthorView.fxml");
    }

    @FXML
    void userButtonPressed(ActionEvent event) {
        loadSubView("../views/UserView.fxml");
    }

    @FXML
    void logoutButtonPressed(ActionEvent event) {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources){

    }
}
